package cpe.asi2.service;


import cpe.asi2.model.EsbMessageModel;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class EsbSenderService {

    private final JmsTemplate jmsTemplate;

    public EsbSenderService(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    public <T> void sendMessage(String queueName, String httpMethod, T message) {
        EsbMessageModel<T> messageModel = new EsbMessageModel<T>(httpMethod, message);

        jmsTemplate.convertAndSend(queueName, messageModel.serializeMessage());
    }

    public <T> void sendMessage(String queueName, String httpMethod, T message, String transactionId) {
        EsbMessageModel<T> messageModel = new EsbMessageModel<T>(httpMethod, message ,transactionId);
        jmsTemplate.convertAndSend(queueName, messageModel.serializeMessage());
    }

    public <T> void sendNotif(String message, T data, Integer userId) {
        EsbMessageModel<T> messageModel = new EsbMessageModel<T>(message, data, userId);
        jmsTemplate.convertAndSend("notif", messageModel.serializeMessage());
    }
}
