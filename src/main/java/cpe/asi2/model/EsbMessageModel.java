package cpe.asi2.model;


import com.fasterxml.jackson.databind.ObjectMapper;

public class EsbMessageModel<T> {

    private String message;
    private T data;

    private Integer userId = null;
    private String transactionId = null;

    public EsbMessageModel() {

    }

    public EsbMessageModel(String httpMethod, T message) {
        this.message = httpMethod;
        this.data = message;
    }
    public EsbMessageModel(String httpMethod, T message, Integer userId) {
        this.message = httpMethod;
        this.data = message;
        this.userId= userId;
    }

    public EsbMessageModel(String httpMethod, T message, String transactionId) {
        this.message = httpMethod;
        this.data = message;
        this.transactionId = transactionId;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }

    public <U> U getData(Class<U> clazz) {
        return new ObjectMapper().convertValue(data, clazz);
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "MessageModel [httpMethod=" + message + ", message=" + data + ", userId=" + userId +  ", transactionId=" + transactionId +  "]";
    }

    public String serializeMessage() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.writeValueAsString(this);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static <T> EsbMessageModel<T> parseMessage(String json, Class<T> clazz) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(json,
                    objectMapper.getTypeFactory().constructParametricType(EsbMessageModel.class, clazz));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
